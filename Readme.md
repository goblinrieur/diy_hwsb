# Idea

NO LICENCE feel free to use, participate or fork it.

The idea of this project is to build end-to-end a very basic sequenced sound box from very cheap & common chips.

Typically this will run around two ne555 & one CD4017 chip.

This kind of projects already exists, of course, but I wanna have mine.

- v0.0 : achieved Prototype OK

![schematic](Kicad/HWSB/schematic.png)

- v0.1 : current  low size PCB ready

![here](Kicad/HWSB/HWSB.jpg)

- v0.3 : 

![here](Kicad/HWSB/implantation_face_dessus.jpg)

other pics here : 

[pdf print all layers](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Kicad/HWSB/HWSB.pdf)

[size of the new pcb](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Kicad/HWSB/HWSB_XY_SIZE_M2_holes.jpg)

[thickness of componnents](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Kicad/HWSB/HWSB_epais.jpg)

[UP face](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Kicad/HWSB/implatation_face_dessus.jpg)

[DOWN face](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Kicad/HWSB/HWSB_dessous.jpg)

- v1.0 : next step : design a box to be 3D printed

## Professional ? 

Of course not. And I don't want to make something complicated.

Where to build PCB ? send Gerber files & drill files to [JLCPCB](https://jlcpcb.com/)

## External contact

[contact me](mailto:goblinrieur@gmail.com) by mail or [discord](https://discord.gg/9szvduB)

[youtube](https://www.youtube.com/channel/UCnsW_UH9vXX-nBe3X-enXYA?view_as=subscriber)

## inside a box ?

Maybe a 3D printable box would be build for this project after prototyping phase.

First prototype will use a simple front panel box designed from [Qcad](https://qcad.org/en/)

## Next step 

design a new box to be 3D printed in OAAC tool,  Object As A Code.

This box will not use screws but a 3D printed rail to put the PCB in place

### design a new box to be 3D printed in OAAC tool,  Object As A Code.

the tool would probably be [Openscad](https://www.openscad.org/)

Layer/slicer would probably be [cura](https://ultimaker.com/fr/software/ultimaker-cura)

## some code ?

Maybe some documentations & Gerber file would be build & distributed.

Maybe some [graphviz](https://www.graphviz.org/) as documentation of project would be made soon.

## EasyEDA & KiCad 

Why this ?  Because those products have quite easy learning..  [KiCad](https://www.kicad.org/)

I use 5.1.x version.

# inspiration ?

see this link [555-timer-circuits](http://www.instructables.com/id/Music-Box-Circuit-from-555-timer-circuitscom/)

## 2018-07-24 first try for time base 

too many noise, signal not so good ... may simulate better values for resistor & capacitor to get something
correct, probably better with  100K instead of 22K to get correct master frequency.

## 2018-08-23 Try to perform a first (0.0) version with PCB & new electronic design

to be able to indicate properly which potentiometer "tone" is currently playing, I modified the whole design
built a net-list, & made a PCB (routed) with all drill & all needed Gerber files.

## 2020-09-04 new PCB design & new pics in this doc.

## pics of older version of the PCB

[face up](https://gitlab.com/goblinrieur/diy_hwsb/-/tree/master/Kicad/HWSB/implatation_face_dessus.jpg "up face")

[route](https://gitlab.com/goblinrieur/diy_hwsb/-/tree/master/Kicad/HWSB/routage_plaquette.png "schematic")

[box prototype design test](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Qcad/prototyping_plastic_face_seems_OK.jpg "design test")

[box real design](https://gitlab.com/goblinrieur/diy_hwsb/-/blob/master/Qcad/box-design_front_panlel.bmp "design of box")

